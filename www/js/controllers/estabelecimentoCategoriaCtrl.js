'use strict';
app.controller("EstabelecimentoCategoriaCtrl", ['$scope', '$rootScope' ,'$ionicModal', 'dbFactory', 
    function ($scope, $rootScope, $ionicModal, dbFactory, $state) {

    $scope.loading = false;
    $scope.categorias = [];
    $scope.item = {};

    $scope.openModal = function(){
        $scope.modal.show();
    }

    $scope.hideModal = function(){
        $scope.modal.hide();
    }

    $scope.loadModal = function(){
        $ionicModal.fromTemplateUrl('templates/modais/estabelecimento-categoria.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
    }

    $scope.limpar = function(){
        $scope.item = {};
    }

    $scope.salvar = function(item){
        $scope.loading = true;
        $scope.item = angular.copy(item);

        dbFactory.setCategoriaEstabelecimento($scope.item).then(function(data) {
            $scope.modal.hide();
            $scope.limpar();
            $scope.init();
            $scope.loading = false;
        }, function(error) {
            console.log(error);
            $scope.loading = true;
        });
    }

    $scope.init = function(){
        $scope.loading = true;
        $scope.loadModal();
        
        dbFactory.getCategoriaEstabelecimento().then(function(data){
            
            $scope.categorias = data;
            $scope.loading = false;    
        }, function(error){
            $scope.loading = false;
            console.log(error);
        }); 
           
    }
    $scope.init();
    
    
}]);