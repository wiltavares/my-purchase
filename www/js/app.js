// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
var app = angular.module('app', ['ionic', 'ngCordova', 'ui.utils.masks']);

app.run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
});

app.config(function($stateProvider, $urlRouterProvider) {

    // Ionic uses AngularUI Router which uses the concept of states
    // Learn more here: https://github.com/angular-ui/ui-router
    // Set up the various states which the app can be in.
    // Each state's controller can be found in controllers.js
    $stateProvider

        .state('login', {
        url: "/login",
        templateUrl: "templates/login.html",
        controller: "LoginCtrl"
    })

    // setup an abstract state for the tabs directive
    .state('tab', {
        url: '/tab',
        abstract: true,
        templateUrl: 'templates/tabs.html'
    })

    // Each tab has its own nav history stack:

    .state('tab.listas', {
        url: '/listas',
        views: {
            'tab-listas': {
                templateUrl: 'templates/listas.html',
                controller: 'ListasCtrl'
            }
        }
    })

    .state('tab.listas/lista', {
        url: '/listas/lista',
        views: {
            'tab-listas': {
                templateUrl: 'templates/lista.html',
                controller: 'ListaCtrl'
            }
        }
    })

    .state('tab.produtos', {
            url: '/produtos',
            views: {
                'tab-produtos': {
                    templateUrl: 'templates/produtos.html',
                    controller: 'ProdutoCtrl'
                }
            }
    })
    
    .state('tab.estabelecimentos', {
        url: '/estabelecimentos',
        views: {
            'tab-estabelecimentos': {
                templateUrl: 'templates/estabelecimentos.html',
                controller: 'EstabelecimentoCtrl'
            }
        }
    })

    .state('tab.mais-opcoes', {
            url: '/mais-opcoes',
            views: {
                'tab-mais-opcoes': {
                    templateUrl: 'templates/mais-opcoes.html',
                    controller: 'IndexCtrl'
                }
            }
    })

    .state('tab.produtos/categorias', {
        url: '/produtos/categorias',
        views: {
            'tab-mais-opcoes': {
                templateUrl: 'templates/produto-categorias.html',
                controller: 'ProdutoCategoriaCtrl'
            }
        }
    })

    .state('tab.estabelecimentos/categorias', {
        url: '/estabelecimentos/categorias',
        views: {
            'tab-mais-opcoes': {
                templateUrl: 'templates/estabelecimento-categorias.html',
                controller: 'EstabelecimentoCategoriaCtrl'
            }
        }
    })

    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('login');

});

app.config(['$ionicConfigProvider', function($ionicConfigProvider) {

    $ionicConfigProvider.tabs.position('bottom'); // other values: top

}]);