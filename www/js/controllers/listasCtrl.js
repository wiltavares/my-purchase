'use strict';
app.controller("ListasCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', 'dbFactory', 'ScopeGlobal',
    function($scope, $rootScope, $ionicModal, $state, dbFactory, ScopeGlobal) {
    
    $scope.item = {};
    $scope.listas = [];
    $scope.loading = false;

    $scope.novaLista = function() {
        $state.go('tab.listas/lista');
    }

    $scope.editLista = function(item){
        ScopeGlobal.setItem(item);
        $state.go('tab.listas/lista');
    }

    $scope.convertToDate = function(data){
        return new Date(data)
    }

    $scope.init = function(){
        $scope.loading = true;
        dbFactory.getListas().then(function(data){
            $scope.listas = data;
            $scope.loading = false;
        }, function(error){
            $scope.loading = false;
        });
    }
    $scope.init();
}]);