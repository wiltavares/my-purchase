'use strict';
app.controller("EstabelecimentoCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', 'dbFactory',
    function($scope, $rootScope, $ionicModal, $state, dbFactory) {

        $scope.item = {};
        $scope.estabelecimentos = [];
        $scope.categorias = [];
        $scope.loading = false;

        $scope.salvar = function(item) {
            $scope.loading = true;
            $scope.item = angular.copy(item);
            $scope.item.categoria = $scope.item.categoria.descricao;
            
            var inFile = document.getElementById("file").files[0];

            if(inFile != undefined)
            {
                dbFactory.setImagemEstabelecimento(inFile).then(function(data){
                    
                    $scope.item.imagem = data;

                    dbFactory.setEstabelecimento($scope.item).then(function(data) {
                        $scope.modal.hide();
                        $scope.loading = false;
                        $scope.init();
                    }, function(error) {
                        console.log(error);
                        $scope.loading = false;
                    });

                }, function(error){
                    console.log(error);
                    $scope.loading = false;
                });
            }
            else
            {
                dbFactory.setEstabelecimento($scope.item).then(function(data) {
                    $scope.modal.hide();
                    $scope.loading = false;
                    $scope.init();
                }, function(error) {
                    console.log(error);
                    $scope.loading = false;
                });
            }
        }

        $scope.loadCategorias = function(){
            dbFactory.getCategoriaEstabelecimento().then(function(data){
                $scope.categorias = data;
                $scope.loading = false;    
            }, function(error){
                $scope.loading = false;
                console.log(error);
            }); 
        }

        $scope.loadModal = function(){
            $ionicModal.fromTemplateUrl('templates/modais/estabelecimento.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
            });
        }

        $scope.openModal = function(){
            $scope.modal.show();
        }

        $scope.hideModal = function(){
            $scope.modal.hide();    
        }

        $scope.openFileUpload = function(){
            document.getElementById("file").click()
        }

        $scope.init = function(){
            $scope.loading = true;

            dbFactory.getEstabelecimento().then(function(data){
                
                $scope.estabelecimentos = data;
                $scope.loadModal();
                $scope.loadCategorias();
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });  
        }
        $scope.init();

    }
]);

function fileChange(sender)
{
    var fReader = new FileReader();
    fReader.readAsDataURL(sender.files[0]);
    fReader.onloadend = function(event){
        var img = document.getElementById("previewImage");
        img.src = event.target.result;
    }
}