app.factory('dbFactory', function($q) {

    var config = {
        apiKey: "AIzaSyCerKKiJrOymTYOzKYb6QulYr1CfF6IFXQ",
        authDomain: "mypurchase-8902e.firebaseapp.com",
        databaseURL: "https://mypurchase-8902e.firebaseio.com",
        storageBucket: "mypurchase-8902e.appspot.com",
        messagingSenderId: "1087722975839"
    };
    firebase.initializeApp(config);

    var service = {};
    var database = firebase.database();
    var storage = firebase.storage();

    service.getsCategorias = function() {
        var deferred = $q.defer();
        var sCategorias = [];

        database.ref('sCategorias/').once('value').then(function(item) {
                sCategorias = item.val();
                deferred.resolve(sCategorias);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;
    }

    service.getSupermercado = function(){
        var deferred = $q.defer();
        var supermercados = [];

        database.ref('estabelecimentos/Supermercado').once('value').then(function(item) {
                supermercados = item.val();
                deferred.resolve(supermercados);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;    
    }

    service.setsCategoria = function(item) {
        var deferred = $q.defer();

        database.ref().child('sCategorias/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;
    }

    service.setSupermercado = function(item) {
        var deferred = $q.defer();

        database.ref().child('estabelecimentos/'+ item.categoria).push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;
    }

    service.setUsuario = function(email, password){
        var deferred = $q.defer();

        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result){
            deferred.resolve("Usuário Cadastrado!");    
        }).catch(function(error) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + error.code + " - " + error.message);    
        });

        return deferred.promise;
    }

    service.logar = function(email, password){
        var deferred = $q.defer();

        firebase.auth().signInWithEmailAndPassword(email, password).then(function(result){
            deferred.resolve(result);     
        }).catch(function(error) {
             deferred.reject("Falha ao executar a instrução, motivo:\n " + error.code + " - " + error.message);
         });

         return deferred.promise;
    }

    service.currentUser = function(){
        var deferred = $q.defer();

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                deferred.resolve(user);  
            } else {
                deferred.resolve(null);
            }
        });

        return deferred.promise;
    }

    service.setImagemSupermercado = function(file) {
        var deferred = $q.defer();
        var storageRef = storage.ref();

        var metadata = {
            contentType: 'image/jpeg'
        };

        var uploadTask = storageRef.child('estabelecimentos/' + file.name).put(file, metadata);

        uploadTask.on('state_changed', function(snapshot){
           console.log(snapshot);
        }, function(error) {
            deferred.reject(error);
        }, function() {
            deferred.resolve(uploadTask.snapshot.downloadURL);
        });

        return deferred.promise;
    }

    return service;
});