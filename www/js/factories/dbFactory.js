var service = {};
app.factory('dbFactory', function($q) {

    var config = {
        apiKey: "AIzaSyCerKKiJrOymTYOzKYb6QulYr1CfF6IFXQ",
        authDomain: "mypurchase-8902e.firebaseapp.com",
        databaseURL: "https://mypurchase-8902e.firebaseio.com",
        storageBucket: "mypurchase-8902e.appspot.com",
        messagingSenderId: "1087722975839"
    };
    firebase.initializeApp(config);

    
    var database = firebase.database();
    var storage = firebase.storage();

    service.getsCategorias = function() {
        var deferred = $q.defer();
        var sCategorias = [];

        database.ref('sCategorias/').once('value').then(function(item) {
                sCategorias = item.val();
                deferred.resolve(sCategorias);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;
    }

    service.getEstabelecimento = function(){
        var deferred = $q.defer();
        var estabeleciementos = [];

        database.ref('estabelecimentos/').once('value').then(function(item) {
                estabeleciementos = item.val();
                deferred.resolve(estabeleciementos);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;    
    }

    service.setCategoriaEstabelecimento = function(item) {
        var deferred = $q.defer();

        database.ref().child('categorias/estabelecimento/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;
    }

    service.getCategoriaEstabelecimento = function(){
        var deferred = $q.defer();
        var categorias = [];

        database.ref('categorias/estabelecimento/').once('value').then(function(item) {
                categorias = item.val();
                deferred.resolve(categorias);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;       
    }

    service.setEstabelecimento = function(item) {
        var deferred = $q.defer();
        
        database.ref().child('estabelecimentos/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;
    }

    service.setProduto = function(item){
        var deferred = $q.defer();
        
        database.ref().child('produtos/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;    
    }

    service.getProdutos = function(){
        var deferred = $q.defer();
        var produtos = [];

        database.ref('produtos/').once('value').then(function(item) {
                produtos = item.val();
                deferred.resolve(produtos);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;           
    }

    service.setCategoriaProduto = function(item){
        var deferred = $q.defer();
        
        database.ref().child('categorias/produtos/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;        
    }

    service.getCategoriaProduto = function(){
        
        var deferred = $q.defer();
        var categorias = [];

        database.ref('categorias/produtos/').once('value').then(function(item) {
                categorias = item.val();
                deferred.resolve(categorias);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;       
    }

    service.setLista = function(item){
        var deferred = $q.defer();
        
        database.ref().child('listas/').push(item).then(function(result) {
            deferred.resolve("Registro inserido!");
        }).catch(function(result) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
        });
        return deferred.promise;
    }

    service.getListas = function(){
        var deferred = $q.defer();
        var listas = [];

        database.ref('listas/').once('value').then(function(item) {
                listas = item.val();
                deferred.resolve(listas);
            })
            .catch(function(result) {
                deferred.reject("Falha ao executar a instrução, motivo:\n " + result.message);
            });
        return deferred.promise;         
    }

    service.setUsuario = function(email, password){
        var deferred = $q.defer();

        firebase.auth().createUserWithEmailAndPassword(email, password).then(function(result){
            deferred.resolve("Usuário Cadastrado!");    
        }).catch(function(error) {
            deferred.reject("Falha ao executar a instrução, motivo:\n " + error.code + " - " + error.message);    
        });

        return deferred.promise;
    }

    service.logar = function(email, password){
        var deferred = $q.defer();

        firebase.auth().signInWithEmailAndPassword(email, password).then(function(result){
            deferred.resolve(result);     
        }).catch(function(error) {
             deferred.reject("Falha ao executar a instrução, motivo:\n " + error.code + " - " + error.message);
         });

         return deferred.promise;
    }

    service.currentUser = function(){
        var deferred = $q.defer();

        firebase.auth().onAuthStateChanged(function(user) {
            if (user) {
                deferred.resolve(user);  
            } else {
                deferred.resolve(null);
            }
        });

        return deferred.promise;
    }

    service.setImagemEstabelecimento = function(file) {
        var deferred = $q.defer();
        var storageRef = storage.ref();

        var metadata = {
            contentType: 'image/jpeg'
        };

        var uploadTask = storageRef.child('estabelecimentos/' + file.name).put(file, metadata);

        uploadTask.on('state_changed', function(snapshot){
           console.log(snapshot);
        }, function(error) {
            deferred.reject(error);
        }, function() {
            deferred.resolve(uploadTask.snapshot.downloadURL);
        });

        return deferred.promise;
    }

    return service;
});