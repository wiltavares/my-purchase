'use strict';
app.controller("ListaCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', function($scope, $rootScope, $ionicModal, $state) {

    $scope.item = {};

    $ionicModal.fromTemplateUrl('templates/lista.html', {
        scope: $scope
    }).then(function(modal) {
        $scope.modal = modal;
    });

    $scope.teste = function() {
        $state.go('tab.listas/nova');
    }
}]);