app.service('ScopeGlobal', function() {
    var item = {};
    var user = {};
    return {
        getItem: function() {
            return item;
        },
        setItem: function(value) {
            item = value;
        },

        getUser: function() {
            return user;
        },
        setUser: function(value) {
            user = value;
        }
    };
});