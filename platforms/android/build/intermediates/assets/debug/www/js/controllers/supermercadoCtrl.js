'use strict';
app.controller("SupermercadoCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', 'dbFactory',
    function($scope, $rootScope, $ionicModal, $state, dbFactory) {

        $scope.item = {};
        $scope.supermercados = [];
        $scope.categorias = [];
        $scope.loading = false;
        

        $scope.salvar = function(item) {
            $scope.loading = true;
            $scope.item = angular.copy(item);
            
            var inFile = document.getElementById("file").files[0];

            dbFactory.setImagemSupermercado(inFile).then(function(data){
                
                $scope.item.imagem = data;

                $scope.item.categoria = $scope.item.categoria.descricao;

                dbFactory.setSupermercado($scope.item).then(function(data) {
                    $scope.modal.hide();
                    $scope.limpar();
                    $scope.init();
                    $scope.loading = false;
                }, function(error) {
                    console.log(error);
                    $scope.loading = true;
                });

            }, function(error){
                console.log(error);
                $scope.loading = true;
            });
        }

        $scope.limpar = function(){
            $scope.item = {};
        }

        $scope.loadCategorias = function(){
            dbFactory.getCategoriaEstabelecimento().then(function(data){
            
                $scope.categorias = data;   
            }, function(error){
                $scope.loading = false;
                console.log(error);
            });     
        }

        $scope.loadModal = function(){
            $ionicModal.fromTemplateUrl('templates/modais/supermercado.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
            });
        }

        $scope.openModal = function(){
            $scope.modal.show();
        }

        $scope.hideModal = function(){
            $scope.modal.hide();
        }

        $scope.init = function(){
            $scope.loading = true;

            dbFactory.getSupermercado().then(function(data){
                $scope.supermercados = data;
                $scope.loadCategorias();
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });  
        }
        $scope.init();
    }
]);