'use strict';
app.controller("LoginCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', 'dbFactory',
    function($scope, $rootScope, $ionicModal, $state, dbFactory) {
    
    $scope.loading = true;
    $scope.item = {};

    $scope.logar = function(item) {
         $scope.item = angular.copy(item);

         if (item.login != '' && item.pw != '') 
        {
            dbFactory.logar(item.login, item.pw).then(function(data){
                $state.go('tab.listas');
            }, function(error){
                console.log(error);
            }); 
        }
    }

    $scope.cadastrar = function(item) {
        $scope.item = angular.copy(item);

        if (item.login != '' && item.pw != '') 
        {
            debugger;
           dbFactory.setUsuario(item.login, item.pw).then(function(data){
               $scope.hideCadastrar();
               $scope.item = {};
           }, function(error){
               console.log(error);
           }); 
        }
    }

    $ionicModal.fromTemplateUrl('templates/modais/usuario.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modalUsuario = modal;
    });

    $scope.showCadastrar = function() {
        $scope.modalUsuario.show();
    };
    $scope.hideCadastrar = function() {
        $scope.modalUsuario.hide();
    };

    $ionicModal.fromTemplateUrl('templates/modais/recuperarSenha.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.modalRecuperarSenha = modal;
    });

    $scope.showRecuperarSenha = function() {
        $scope.modalRecuperarSenha.show();
    };
    $scope.hideRecuperarSenha = function() {
        $scope.modalRecuperarSenha.hide();
    };

    $scope.init = function(){
        $scope.loading = true;
        dbFactory.currentUser().then(function(data){
            
            if (data != null)
                $state.go('tab.listas'); 
            else
                $scope.loading = false; 
        });
    }
    $scope.init();
}]);