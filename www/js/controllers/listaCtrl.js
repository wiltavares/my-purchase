'use strict';
app.controller("ListaCtrl", ['$scope', '$rootScope', '$ionicModal', '$ionicPopup', '$state', 'ScopeGlobal', 'dbFactory',
    function($scope, $rootScope, $ionicModal, $ionicPopup, $state, ScopeGlobal, dbFactory) {

        $scope.loading = false;
        $scope.popup = {};
        $scope.estabelecimentos = [];
        $scope.produtos = [];
        $scope.item = {};
        $scope.item.total = 0;
        $scope.item.produtosSelecionados = [];

        $scope.loadModal = function(){
            $ionicModal.fromTemplateUrl('templates/modais/lista-produtos.html', {
                scope: $scope
            }).then(function(modal) {
                $scope.modal = modal;
            });
        }

        $scope.openModal = function(){
            $scope.modal.show();
        }

        $scope.hideModal = function(){
            $scope.modal.hide();
        }

        $scope.loadEstabelecimentos = function(){
            dbFactory.getEstabelecimento().then(function(data){
                $scope.estabelecimentos = data;
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });  
        }
        
        $scope.loadProdutos = function(){
            dbFactory.getProdutos().then(function(data){
                
                $scope.produtos = data;
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });    
        }

        $scope.checkProduto = function(item){
            
            if ($scope.item.produtosSelecionados.indexOf(item) == -1)
            {
                $scope.popup.quantidade = 0;
                $scope.popup.valor = 0;

                $scope.showPopup(function(data){
                    if (data != undefined)
                    {
                        item.quantidade = data.quantidade;
                        item.valor = data.valor;
                        $scope.item.total += (item.valor * item.quantidade);
                        $scope.item.produtosSelecionados.push(item);
                    }
                    else
                    {
                        item.check = false;
                    }
                    
                });
                
            }      
            else
            {
                $scope.removeSelecionado(item);
            } 
        }

        $scope.onEdit = function(item){
            $scope.popup.quantidade = item.quantidade;
            $scope.popup.valor = item.valor;
            $scope.item.total -= (item.valor * item.quantidade);

            $scope.showPopup(function(data){
                item.quantidade = $scope.popup.quantidade ;
                item.valor = $scope.popup.valor;
                $scope.item.total += (item.valor * item.quantidade);
            }) 
        }

        $scope.concluir = function(){

            for(var key in $scope.produtos){
                if ($scope.item.produtosSelecionados.indexOf($scope.produtos[key]) != -1){
                    $scope.produtos[key].hide = true;    
                }    
            }
            $scope.hideModal();
        }

        $scope.removeSelecionado = function(item){
            item.hide = false;
            item.check = false;
            $scope.item.produtosSelecionados.splice($scope.item.produtosSelecionados.indexOf(item), 1);

            $scope.item.total -= (item.valor * item.quantidade);
            
            if (item.quantidade != undefined)
                delete item.quantidade;
        }

        $scope.finalizar = function(){

            if ($scope.item.estabelecimento != undefined && $scope.item.produtosSelecionados.length > 0)
            {
                $scope.item.data = new Date().toString();

                $scope.item.produtosSelecionados.forEach(function(produto){
                    delete produto.$$hashKey;
                });

                dbFactory.setLista($scope.item).then(function(result){
                    console.log(result);
                }, function(error){
                    console.log(error);
                });
            }
        }

        $scope.showPopup = function(callback) {

            // An elaborate, custom popup
            var myPopup = $ionicPopup.show({
                templateUrl: 'templates/popups/lista-quantidade-produto.html',
                title: 'Quantidade',
                subTitle: 'Informe a quantidade desejada do produto',
                scope: $scope,
                buttons: [
                    { 
                        text: 'Cancelar',
                        type: 'button-assertive'
                    },
                    {
                        text: 'Concluir',
                        type: 'button-positive',
                        onTap: function(e) {
                            return $scope.popup;
                        }
                    }
                ]
            }).then(function(res) {
                callback(res);
            });
        };

        $scope.increment = function(){
            $scope.popup.quantidade += 1;
        }

        $scope.incrementHold = function(){
            $scope.popup.quantidade += 10;
        }

        $scope.decrement = function(){
            if ($scope.popup.quantidade > 0)
                $scope.popup.quantidade -= 1;
        }

        $scope.decremenHold = function(){
            if ($scope.popup.quantidade > 0)
                $scope.popup.quantidade -= 10;
        }


        $scope.init = function(){
            $scope.loading = true;
            $scope.loadModal();
            $scope.loadEstabelecimentos();    
            $scope.loadProdutos();    
            debugger;
            $scope.item = ScopeGlobal.getItem();
            ScopeGlobal.setItem(undefined);
            if ($scope.item == undefined)
            {
                $scope.item = {};
                $scope.item.total = 0;
                $scope.item.produtosSelecionados = [];
            }
                
        }
        $scope.init();

    }
]);