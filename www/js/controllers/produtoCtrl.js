'use strict';
app.controller("ProdutoCtrl", ['$scope', '$rootScope' ,'$ionicModal', '$state', 'dbFactory', 
    function ($scope, $rootScope, $ionicModal, $state, dbFactory) {

    $scope.loading = false;
    $scope.item = {};
    $scope.produtos = [];
    $scope.categorias = [];

    $scope.salvar = function(item){
        $scope.loading = true;
        $scope.item = angular.copy(item);

        dbFactory.setProduto($scope.item).then(function(data){
            $scope.loading = false;
            $scope.limpar();
            $scope.hideModal();
            $scope.init();
            console.log(data);
        }, function(error){
            $scope.loading = false;
            console.log(error);
        });
    }

    $scope.limpar = function(){
        $scope.item = {};
    }

    $scope.loadModal = function(){
        $ionicModal.fromTemplateUrl('templates/modais/produto.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
    }

    $scope.openModal = function(){
        $scope.modal.show();
    }

    $scope.hideModal = function(){
        $scope.modal.hide();
    }

    $scope.loadCategorias = function(){
        dbFactory.getCategoriaProduto().then(function(data){
            $scope.categorias = data;   
        }, function(error){
            $scope.loading = false;
            console.log(error);
        });
    }

    $scope.init = function(){
        $scope.loading = true;
        $scope.loadModal();

        dbFactory.getProdutos().then(function(data){
            $scope.produtos = data;
            $scope.loadCategorias();
            $scope.loading = false;
        }, function(error){
            console.log(error);
            $scope.loading = false;
        });
    }
    $scope.init();
    
    
}]);