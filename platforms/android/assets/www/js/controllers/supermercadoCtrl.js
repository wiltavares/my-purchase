'use strict';
app.controller("SupermercadoCtrl", ['$scope', '$rootScope', '$ionicModal', '$state', 'dbFactory',
    function($scope, $rootScope, $ionicModal, $state, dbFactory) {

        $scope.item = {};
        $scope.supermercados = [];
        $scope.loading = false;

        $scope.categorias = [
            { descricao: 'Hipermercado' },
            { descricao: 'Supermercado' },
            { descricao: 'Mercearia' }
        ];

        $scope.salvar = function(item) {
            $scope.loading = true;
            $scope.item = angular.copy(item);
            
            var inFile = document.getElementById("file").files[0];

            dbFactory.setImagemSupermercado(inFile).then(function(data){
                
                $scope.item.imagem = data;

                $scope.item.categoria = $scope.item.categoria.descricao;

                dbFactory.setSupermercado($scope.item).then(function(data) {
                    $scope.modal.hide();
                    $scope.loading = false;
                }, function(error) {
                    console.log(error);
                    $scope.loading = true;
                });

            }, function(error){
                console.log(error);
                $scope.loading = true;
            });
        }

        $ionicModal.fromTemplateUrl('templates/modais/supermercado.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });



        $scope.init = function(){
            $scope.loading = true;

            dbFactory.getSupermercado().then(function(data){
                $scope.supermercados = data;
                $scope.loading = false;
            }, function(error){
                console.log(error);
                $scope.loading = false;
            });  
        }
        $scope.init();
    }
]);